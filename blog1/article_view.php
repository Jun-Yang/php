<?php

require_once 'db.php';

function get_form($u="") {
// example of 'heredoc'
    $form = <<< ENDOFIT
<h1>User Login</h1>
<form method="post">
    Username: <input type="text" name="username" value="$u"><br>
    Password: <input type="text" name="title" ><br>
    <input type="submit" value="view" >
</form>
ENDOFIT;
    return $form;
}

// State 1: show form
if (isset($_POST['username'])) {
    // extract variables
    $username = $_POST['username'];
    $pass = $_POST['pass'];
    // verify data submitted
    $query = sprintf("SELECT * FROM articles WHERE (username = %s AND title = %s)",
                mysqli_real_escape_string($link,$username),
                mysqli_real_escape_string($link,$username)
                );
    $result = mysqli_query($link, $query);
    if (!$result) {
            echo "Error: executing SQL querry." . PHP_EOL;
            echo "Debugging errno: " . mysqli_errno($link) . PHP_EOL;
            echo "Debugging error: " . mysqli_error($link) . PHP_EOL;
            exit;
    }
    $row = mysqli_fetch_assoc($result); 
    $error = false;
    print_r($row);
    if(!$row){
        $error = true;
    } else { 
        if($pass != $row['password']) {
         $error = true;
        }
    }
    if($error) {
        echo get_form();
        echo "Login credentails invalid, try again.";
    }
    else {
        unset($row['password']);
        $_SESSION['user'] = $row;
        echo "Login successful.";
    }
} else {
    echo get_form();
}    



