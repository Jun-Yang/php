<?php

session_cache_limiter(false);
session_start();

require_once '../vendor/autoload.php';

//DB::$host = '127.0.0.1';
DB::$user = 'quiz2';
DB::$password = 'CvJCnPwvdSb7bK2K';
DB::$dbName = 'quiz2';
DB::$port = 3333;
DB::$encoding = 'utf8';

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/../cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/../templates');

if (!isset($_SESSION['user'])) {
    $_SESSION['user'] = array();
}

$twig = $app->view()->getEnvironment();
$twig->addGlobal('user', $_SESSION['user']);

$app->get('/', function() use ($app) {
    $app->render('index.html.twig');
});

// STATE 1: First show
$app->get('/register', function() use ($app) {
    $app->render('register.html.twig');
});

// Receiving a submission
$app->post('/register', function() use ($app) {
    // extract variables
    $username = $app->request()->post('username');
    $pass1 = $app->request()->post('pass1');
    $pass2 = $app->request()->post('pass2');
    // list of values to retain after a failed submission
    $valueList = array('username' => $username);
    // check for errors and collect error messages
    $errorList = array();
    if (strlen($username)> 20 || strlen($username) < 2) {
        array_push($errorList, "username length is between 2-20");
    } else {
        $user = DB::queryFirstRow("SELECT * FROM users WHERE username=%s", $username);
        if ($user) {
            array_push($errorList, "Username already in use");
        }
    }
    if ($pass1 != $pass2) {
        array_push($errorList, "Passwors do not match");
    } else {
        if (strlen($pass1) < 6) {
            array_push($errorList, "Password too short, must be 6 characters or longer");
        }
        if (preg_match('/[A-Z]/', $pass1) != 1 || preg_match('/[a-z]/', $pass1) != 1 || preg_match('/[0-9]/', $pass1) != 1) {
            array_push($errorList, "Password must contain at least one lowercase, "
                    . "one uppercase letter, and a digit");
        }
    }
    //
    if ($errorList) {
        $app->render('register.html.twig', array(
            'errorList' => $errorList,
            'v' => $valueList
        ));
    } else {
        DB::insert('users', array(
            'username' => $username,
            'password' => $pass1
        ));
        $app->render('register_success.html.twig');
    }
});

// AJAX: Is user with this email already registered?
$app->get('/ajax/emailused/:email', function($email) {
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    //echo json_encode($user, JSON_PRETTY_PRINT);
    echo json_encode($user != null);
});


// HOMEWORK 1: implement login form
$app->get('/login', function() use ($app) {
    $app->render('login.html.twig');
});

$app->post('/login', function() use ($app) {
    $username = $app->request()->post('username');
    $pass = $app->request()->post('pass');
    // verification    
    $error = false;
    $user = DB::queryFirstRow("SELECT * FROM users WHERE username=%s", $username);
    if (!$user) {
        $error = true;
    } else {
        if ($user['password'] != $pass) {
            $error = true;
        }
    }
    // decide what to render
    if ($error) {
        $app->render('login.html.twig', array("error" => true));
    } else {
        unset($user['password']);
        $_SESSION['user'] = $user;
        $app->render('admin_birthday_list.html.twig');
    }
});

// HOMEWORK 2: find and implement any tutorial about PHP file upload.
// create a new pure-PHP project to do it in

$app->get('/admin/birthday/:op(/:id)', function($op, $id = 0) use ($app) {
    /* FOR PROJECTS WITH MANY ACCESS LEVELS
    if (($_SESSION['user']) || ($_SESSION['level'] != 'admin')) {
        $app->render('forbidden.html.twig');
        return;
    } */
    if ($op == 'edit') {
        $birthday = DB::queryFirstRow("SELECT * FROM birthdays WHERE id=%i", $id);
        if (!$birthday) {
            echo 'birthday not found';
            return;
        }
        $app->render("admin_birthday_add.html.twig", array(
            'v' => $birthday, 'operation' => 'Update'
        ));
    } else {
        $app->render("admin_birthday_add.html.twig",
                array('operation' => 'Add'
        ));
    }
})->conditions(array(
    'op' => '(add|edit)',
    'id' => '[0-9]+'));

$app->post('/admin/birthday/:op(/:id)', function($op, $id = 0) use ($app) {
    $name = $app->request()->post('name');
    $month = $app->request()->post('month');
    $day = $app->request()->post('day');
    $ownerId = $_SESSION['user']['id'];
    $valueList = array(
        'ownerId' => $ownerId,
        'name' => $name,
        'month' => $month,
        'day' => $day);
    
    $errorList = array();
    if (strlen($name) < 2 || strlen($name) > 50) {
        array_push($errorList, "Name must be 2-50 characters long");
    }
    if (empty($month) || $month < 1 || $month > 12) {
        array_push($errorList, "Month must be 1-12");
    }
    if (empty($day) || $day < 1 || $day > 31) {
        array_push($errorList, "Day must be 1-31");
    }
    
    //
    if ($errorList) {
        $app->render("admin_birthday_add.html.twig", array(
            'v' => $valueList,
            "errorList" => $errorList,
            'operation' => ($op == 'edit' ? 'Edit' : 'Update')
        ));
    } else {
        if ($op == 'edit') {
            DB::update('birthdays', array(
                "ownerId" => $ownerId,
                "name" => $name,
                "month" => $month,
                "day" => $day
            ), "id=%i", $id);
            } else {
            DB::insert('birthdays', array(
                "ownerId" => $ownerId,
                "name" => $name,
                "month" => $month,
                "day" => $day
            ));
        }
        $app->render("admin_birthday_add_success.html.twig", array(
            "name" => $name
        ));
    }
})->conditions(array(
    'op' => '(add|edit)',
    'id' => '[0-9]+'));

// HOMEWORK: implement a table of existing birthdays with links for editing
$app->get('/admin/birthday/list', function() use ($app) {
    $birthdayList =  DB::query("SELECT * FROM birthdays");
    $app->render("admin_birthday_list.html.twig", array(
        'birthdayList' => $birthdayList
    ));
});

$app->get('/admin/birthday/delete/:id', function($id) use ($app) {
    $birthday = DB::queryFirstRow('SELECT * FROM birthdays WHERE id=%i', $id);
    $app->render('admin_birthday_delete.html.twig', array(
        'b' => $birthday
    ));
});

$app->post('/admin/birthday/delete/:id', function($id) use ($app) {
    DB::delete('birthdays', 'id=%i', $id);
    $app->render('admin_birthday_delete_success.html.twig');
});

$app->get('/logout', function() use ($app) {
    unset($_SESSION['eshopuser']);
//    session_destroy();
    $app->render('logout.html.twig');
});

$app->run();
