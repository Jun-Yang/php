<?php

/* hello.html.twig */
class __TwigTemplate_e78f0cb5d532ff3085c0b4a309dfccfa0c4d9564b82ecefe2e0864d6ab041460 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<p>Hello ";
        // line 3
        echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
        echo " (from Twig), you are ";
        echo twig_escape_filter($this->env, ($context["age"] ?? null), "html", null, true);
        echo " y/o.</p>
";
    }

    public function getTemplateName()
    {
        return "hello.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 3,  19 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# empty Twig template #}

<p>Hello {{ name }} (from Twig), you are {{ age }} y/o.</p>
", "hello.html.twig", "C:\\xampp\\htdocs\\php\\slimfirst\\templates\\hello.html.twig");
    }
}
