<?php

require_once 'db.php';
if(!isset($_SESSION['user'])) {
    echo "<h1>Access denied</h1>\n";
    echo "<p>You must be logged in to create the article</p>";
    exit();
}

function get_articleAdd_form($t="", $b="") {
// example of 'heredoc'
    return <<< ENDOFIT
<h1>Add Articles</h1>
<form method="post">
    Title: <input type="text" name="title" value="$t"><br>
    Body: <textarea name='body'></textarea><br>
    <input type="submit" value="Add article" >
</form>
ENDOFIT;
}

// State 1: show form
if (isset($_POST['title'])) {
    // extract variables
    $userid = $_SESSION['user']['id'];
    $title = $_POST['title'];
    $body = $_POST['body'];
    echo $userid, " ", $title, " ", $body, "<br>";
    // verify data submitted
    $errorList = array();

    if (strlen($title) < 5 || strlen($title) > 200) {
        array_push($errorList, "Title must be between 5 and 250 characters.");
    }
    
    if (strlen($body) < 5 || strlen($body) > 2000) {
        array_push($errorList, "Content must be between 6 and 2000 characters.");
    }
    
    /* avoid SQL injection */
    if (!$errorList) {//Stae 2: successful submission
        $query = sprintf("INSERT INTO articles VALUES (NULL, '%s', '%s', '%s')",
                mysqli_real_escape_string($link,$userid),
                mysqli_real_escape_string($link,$title),
                mysqli_real_escape_string($link,$body)
                );
        
        $result = mysqli_query($link, $query);

        if (!$result) {
            echo "Error: executing SQL querry." . PHP_EOL;
            echo "Debugging errno: " . mysqli_errno($link) . PHP_EOL;
            echo "Debugging error: " . mysqli_error($link) . PHP_EOL;
            exit;
        }
        $id = mysqli_insert_id($link);
        echo '<p>Success! You may now <a href=\"article_view.php?id=$id\">"
                . "View your article</a> . </p>';
    } else {
        //State 3: failed submission
        echo get_articleAdd_form($title,$body);
        echo '<p class="error">Error in your submission:</p>';
        echo "\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li>$error</li>";
        }
        echo "</ul>\n";
    }
} else {
    echo get_articleAdd_form();
}

