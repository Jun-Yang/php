<?php

require_once 'db.php';
//if(!isset($_SESSION['name'])) {
//    echo "<h1>Access denied</h1>\n";
//    echo "<p>You must be logged in to create the article</p>";
//    exit();
//}

function get_form($t="") {
// example of 'heredoc'
    return <<< ENDOFIT
<form method="post">
    <h1>Add task</h1>
    <a href="index.php">Display task</a><br>
    Task: <input type="text" name="task" value="$t"><br>
    DueDate: <input type="text" name="dueDate" ><br>
    <input type="submit" value="Add task" ><br>
</form>
ENDOFIT;
}

// State 1: show form
if (isset($_POST['task'])) {
    // extract variables
    $task = $_POST['task'];
    $dueDate = $_POST['dueDate'];
//    echo $task, " ", $dueDate, "<br>";
    // verify data submitted
    $errorList = array();

    if (preg_match("/^[0-9a-zA-Z\_\-\s]{3,100}$/", $task)!= 1) {
        array_push($errorList, "Tast must be between 3 and 100 characters, must use legal characters.");
    }
    
    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$dueDate)!= 1) {
        array_push($errorList, "DueDate must be YYYY-MM-DD.");
    }
    
    /* avoid SQL injection */
    if (!$errorList) {//Stae 2: successful submission
        $query = sprintf("INSERT INTO todos VALUES (NULL, '%s', '%s')",
                mysqli_real_escape_string($link,$task),
                mysqli_real_escape_string($link,$dueDate)
                );
        
        $result = mysqli_query($link, $query);

        if (!$result) {
            echo "Error: executing SQL querry." . PHP_EOL;
            echo "Debugging errno: " . mysqli_errno($link) . PHP_EOL;
            echo "Debugging error: " . mysqli_error($link) . PHP_EOL;
            exit;
        }
        $_SESSION['count']++;
         echo get_form();

    } else {
        //State 3: failed submission
        echo get_form($task);
        echo '<p class="error">Error in your submission:</p>';
        echo "\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li>$error</li>";
        }
        echo "</ul>\n";
    }
} else {
    echo get_form();
}

printf("You add %s task in this session.", $_SESSION['count']) ;

