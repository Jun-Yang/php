<form>
    What is your name?  <input type="text" name="name"><br>
    What is your age?   <input type="number" name="age"><br>
    <input type="submit" value="Insert data">
</form>

<?php
// password: S2BMNm3lLVYg3Ovm
$result = false;
$dbhost = "localhost:3333";
$username = "hellodb";
$password = "S2BMNm3lLVYg3Ovm";
$database_name = "hellodb";

$conn = mysqli_connect($dbhost, $username, $password, $database_name);
if (mysqli_connect_errno()) {
    die("Connection failed: " . mysqli_connect_error());
}
 
//inserting a record
if (isset($_GET['name']) and isset($_GET['age'])) {
    $name = $_GET['name'];
    $age = $_GET['age'];

    /* @var $sql type */
    $sql = "INSERT INTO persons (name, age) VALUES('". $name ."', '". $age ."')";
    if (mysqli_query($conn, $sql)) {
        print "Success! ID of last inserted record is : " . mysqli_insert_id($conn) .  "<br />";
    } else {
        /* @var $mysqli_errno type */
        die("Error : (" . $mysqli_errno($conn) . ") " . mysqli_errno($conn));
    }
}

$sql = "SELECT * FROM persons";
$result = $conn->query($sql);

echo "<ul>\n";
if ($result->num_rows > 0) {
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        $name = $row['name'];
        $age = $row['age'];
        echo "<li>$name is $age y/o</li>\n";
        // echo "id: " . $row["id"] . " - Name: " . $row["name"] . " - Age:  " . $row["age"] . "<br>";
    }
} else {
    echo "0 results";
}
echo "<ul>\n";
$conn->close();
