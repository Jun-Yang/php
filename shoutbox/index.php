<?php

session_start();

$db_host ="localhost";
$db_user ="shoutbox";
// put your code here o6kXhXiBWoimfVYG
$db_pass = "o6kXhXiBWoimfVYG";
$db_name ="shoutbox";
$db_port = 3333;

$link = mysqli_connect($db_host, $db_user, $db_pass, $db_name, $db_port );

if (!$link) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

//if(!isset($_SESSION['name'])) {
//    echo "<h1>Access denied</h1>\n";
//    echo "<p>You must be logged in to create the article</p>";
//    exit();
//}

function get_form($n="") {
// example of 'heredoc'
    return <<< ENDOFIT
<form method="post">
    Name: <input type="text" name="name" value="$n"><br>
    Message: <textarea name='message'></textarea>
    <input type="submit" value="Shout!" ><br>
</form>
ENDOFIT;
}

// State 1: show form
if (isset($_POST['name'])) {
    // extract variables
    $name = $_POST['name'];
    $message = $_POST['message'];
    echo $name, " ", $message, "<br>";
    // verify data submitted
    $errorList = array();

    if (!preg_match("/^[0-9a-zA-Z\_\-\s]{3,20}$/", $name)) {
        array_push($errorList, "Name must be between 3 and 20 characters.");
    }
    
    if (strlen($message) < 1 || strlen($message) > 200) {
        array_push($errorList, "Message must be between 1 and 200 characters.");
    }
    
    /* avoid SQL injection */
    if (!$errorList) {//Stae 2: successful submission
        $query = sprintf("INSERT INTO shout VALUES (NULL, '%s', '%s')",
                mysqli_real_escape_string($link,$name),
                mysqli_real_escape_string($link,$message)
                );
        
        $result = mysqli_query($link, $query);

        if (!$result) {
            echo "Error: executing SQL querry." . PHP_EOL;
            echo "Debugging errno: " . mysqli_errno($link) . PHP_EOL;
            echo "Debugging error: " . mysqli_error($link) . PHP_EOL;
            exit;
        }
        $_SESSION['count']++;

    } else {
        //State 3: failed submission
        
        echo '<p class="error">Error in your submission:</p>';
        echo "\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li>$error</li>";
        }
        echo "</ul>\n";
    }
    echo get_form($name);
} else {
    echo get_form();
    if(!isset($_SESSION['count'])){
        $_SESSION['count']= 0;
    }
}

$query = sprintf("SELECT * FROM shout ORDER BY '%s' DESC LIMIT 10",
                mysqli_real_escape_string($link,"id"));
$query = "SELECT * FROM shout ORDER BY id DESC LIMIT 10";
$result = mysqli_query($link, $query);
if (!$result) {
        echo "Error: executing SQL querry." . PHP_EOL;
        echo "Debugging errno: " . mysqli_errno($link) . PHP_EOL;
        echo "Debugging error: " . mysqli_error($link) . PHP_EOL;
        exit;
}
$row = mysqli_fetch_assoc($result); 
echo "\n<ul>\n";
foreach ($result as $row) {
    echo "<li>" . $row['id'] . " " . $row['name'] . " " . $row['message'] . "</li>";
}
echo "</ul>\n";

echo "Successful shout in this session is:" . " " .  $_SESSION['count'];
