<?php

require_once 'db.php';

function get_form($u="", $e="") {
// example of 'heredoc'
    $form = <<< ENDOFIT
<h1>User Registration</h1>
<form method="post">
    Username: <input type="text" name="username" value="$u"><br>
    Email: <input type="email" name="email" value="$e"><br>
    Password: <input type="password" name="pass1" ><br>
    Password(repeated): <input type="password" name="pass2" ><br>
    <input type="submit" value="Register" >
</form>
ENDOFIT;
    return $form;
}

// State 1: show form
if (isset($_POST['username'])) {
    // extract variables
    $username = $_POST['username'];
    $email = $_POST['email'];
    $pass1 = $_POST['pass1'];
    $pass2 = $_POST['pass2'];
    // verify data submitted
    $errorList = array();

    if (strlen($username) < 6 || strlen($username) > 50) {
        array_push($errorList, "Username must be between 6 and 50 characters.");
    }
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
        array_push($errorList, "Email not valid!");
    }
    if (strlen($pass1) < 6 || strlen($pass1) > 100) {
        array_push($errorList, "Password length must be between 6 and 100 characters.");
    }
    if (strlen($pass2) < 6 || strlen($pass2) > 100) {
        array_push($errorList, "Password length must be between 6 and 100 characters.");
    }
    if ($pass1 != $pass2) {
        array_push($errorList, "Passwords do not match.");
    }
    //TODO: check username is composed of only lowercase letters and numbers
    //TODO: check password contains required variaty of charecters
    //TODO:check email not already in use
    //TODO: check user name not already on use
    //
    if (!$errorList) {//Stae 2: successful submission
        $query = sprintf("INSERT INTO users VALUES (NULL, '%s', '%s', '%s')",
                mysqli_real_escape_string($link,$username),
                mysqli_real_escape_string($link,$email),
                mysqli_real_escape_string($link,$pass1)
                );
        echo "$query<br>";
        $result = mysqli_query($link, $query);

        if (!$result) {
            echo "Error: executing SQL querry." . PHP_EOL;
            echo "Debugging errno: " . mysqli_errno($link) . PHP_EOL;
            echo "Debugging error: " . mysqli_error($link) . PHP_EOL;
            exit;
        }
        echo '<p>Success! You may now <a href="login.php">Login</a></p>';
        } else {
            //State 3: failed submission
            echo get_form($username,$email);
            echo '<p class="error">Error in your submission:</p>';
            echo "\n<ul>\n";
            foreach ($errorList as $error) {
                echo "<li>$error</li>";
            }
            echo "</ul>\n";
        }
    } else {
        echo get_form();
    }