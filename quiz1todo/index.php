<!DOCTYPE html>
<html>
    <head>
        <style>
            table, th, td {
                border: 1px solid black;
                padding: 5px;
                width: 400px;
            }
            th, td {
                width:200px;
            }
        </style>

    <h1>Display task</h1> 
    <a href="addtodo.php">Add task</a>
    <form method="post">
        <table>
            <tr>
                <th>Task</th>
                <th>DueDate</th> 
            </tr>
    
    <?php
    require_once 'db.php';

    $query = "SELECT * FROM todos ORDER BY dueDate ASC ";
    $result = mysqli_query($link, $query);
    if (!$result) {
        echo "Error: executing SQL querry." . PHP_EOL;
        echo "Debugging errno: " . mysqli_errno($link) . PHP_EOL;
        echo "Debugging error: " . mysqli_error($link) . PHP_EOL;
        exit;
    }
    $row = mysqli_fetch_assoc($result);
    $dueCount = 0;
    foreach ($result as $row) {
        $today = date("Y-m-d");

        if ($row['dueDate'] < $today) {
            $dueCount++;
            echo '<tr bgcolor="#FF0000">';
        }
        else {
            echo "<tr>";
        }
        echo "<td>{$row['task']}</td>";
        echo "<td>{$row['dueDate']}</td>";
        echo "</tr>";
    }
    
    echo "</table>";
    echo "</form>";
    printf("You have %s task overdue.", $dueCount);
    
    